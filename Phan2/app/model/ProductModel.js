export class Product {
    constructor(id,name, type,price,screen,frontCamera,backCamera,quantity,img,desc) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
        this.screen = screen;
        this.frontCamera = frontCamera;
        this.backCamera = backCamera;
        this.quantity = quantity;
        this.img = img;
        this.desc = desc;
    }

    
}